export default class GMaps {
	/**
	 * url para calcular distancia entre pontos, pode ser mais te um ponto
	 */
	static get URL_CALCULE_DISTANCE () {
		return `https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyD4UEzEtWw-AaEhpiJbU3oNNg1-SkMAT0Y`
	}

	static buildAddress (addressJSON) {
		return `${addressJSON.number}+${addressJSON.street}+${addressJSON.neighborhood}+${addressJSON.city}+${addressJSON.state}`
	}

	/**
	 * Para calcular a distancia é necessário indicar distancia de partida e local final de chegada
	 * pode ser informado tanto o endereço completo quanto latitude e longitude
	 * @param  string addressStart [description]
	 * @param  string addressDestine   [description]
	 * @return {[type]}               [description]
	 */
	static calculateDistanceGenerateURI (addressStart, addressDestine) {
		return `${GMaps.URL_CALCULE_DISTANCE}&origins=${addressStart}&destinations=${addressDestine}/`
	}
}