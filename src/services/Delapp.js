export default class Delapp {
  static get URL_PATH () {
    return 'http://localhost:8000/api/'
  }

  // establishment
  static get URL_PATH_ESTABLISHMENT () {
    return Delapp.URL_PATH + "establishment/"
  }

  static URL_PATH_ESTABLISHMENT_ADDRESS (establishmentId) {
    return Delapp.URL_PATH_ESTABLISHMENT + establishmentId + "/address"
  }

  static URL_PATH_ESTABLISHMENT_CONTACT (establishmentId) {
    return Delapp.URL_PATH_ESTABLISHMENT + establishmentId + "/contact"
  }

  static URL_PATH_ESTABLISHMENT_CLIENT_ADDRESS (establishmentId, clientId) {
    return Delapp.URL_PATH_ESTABLISHMENT + establishmentId + "/client/" + clientId + "/address"
  }

  static get URL_PATH_ESTABLISHMENT_AUTH () {
    return Delapp.URL_PATH_ESTABLISHMENT + "auth"
  }

  // @POST
  static URL_PATH_ESTABLISHMENT_ORDERS (establishmentId) {
    return Delapp.URL_PATH_ESTABLISHMENT + establishmentId + '/orders'
  }

  // deliveryman
  static get URL_PATH_DELIVERYMAN () {
    return Delapp.URL_PATH + "deliveryman/"
  }

  // @GET
  static URL_PATH_DELIVERYMAN_ORDERS (deliverymanId) {
    return Delapp.URL_PATH_DELIVERYMAN + deliverymanId + '/orders'
  }

  // @POST
  static URL_PATH_DELIVERYMAN_ORDERS_ACCEPT (deliverymanId, orderId) {
    return Delapp.URL_PATH_DELIVERYMAN + deliverymanId + '/orders/' + orderId + '/accept'
  }
}