import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Entrar from '@/components/Estabelecimento/Entrar'
import Cadastrar from '@/components/Estabelecimento/Cadastrar'

// Admin estabelecimento
import Interface from '@/components/AdminEstabelecimento/Interface'
import ListarPedidos from '@/components/AdminEstabelecimento/ListarPedidos'

// Admin entregador
import InterfaceDeliveryman from '@/components/AdminEntregador/Interface'
import EntregaLista from '@/components/AdminEntregador/Entregas/Lista'
import EntregaAceita from '@/components/AdminEntregador/Entregas/Aceita'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/estabelecimento/entrar',
      name: 'estabelecimentoEntrar',
      component: Entrar
    },
    {
      path: '/estabelecimento/cadastrar',
      name: 'estabelecimentoCadastrar',
      component: Cadastrar
    },

    // ****** rotas admin ******
    {
      path: '/admin',
      name: 'adminEstabelecimento',
      component: Interface,
      children: [
        {
          path: 'pedidos',
          name: 'adminEstabelecimentoListarPedidos',
          component: ListarPedidos
        }
      ]
    },

    // ****** rotas admin entregador ******
    {
      path: '/entregador',
      name: 'adminEntregador',
      component: InterfaceDeliveryman,
      children: [
        {
          path: 'entregas',
          name: 'adminEntregadorEntregas',
          component: EntregaLista
        },
        {
          path: 'entrega-aceita',
          name: 'adminEntregadorEntregaAceita',
          component: EntregaAceita
        }
      ]
    }
  ]
})
