import Vue from 'vue'
import Vuex from 'vuex'
import analytics from './modules/analytics'
import backupEstablishment from './modules/backupEstablishment'
import novoPedidoDialog from './modules/novoPedidoDialog'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    analytics,
    backupEstablishment,
    novoPedidoDialog
  }
})