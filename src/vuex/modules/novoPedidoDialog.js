const state = {
  show: false
}

const getters = {
  show: state => state.show
}

const mutations = {
  showDialog (state) {
    return state.show = true
  },

  hideDialog (state) {
    return state.show = false
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations
}
