import EstablishmentConstants from './../../contants/Establishment'

const state = {
  backupEstablishment: JSON.parse(localStorage.getItem(EstablishmentConstants.LOCALSTORAGE_BACKUP_ESTABLISHMENT)) || null,
  logged: "false"
}

const getters = {
  backup: state => state.backupEstablishment,
  logged: state => state.logged
}

const mutations = {
  removeBackup (state) {
    localStorage.removeItem(EstablishmentConstants.LOCALSTORAGE_BACKUP_ESTABLISHMENT)
    state.backupEstablishment = null
  },

  addBackup (state, objectBackupEstablishment) {
    let stringObj = JSON.stringify(objectBackupEstablishment)
    localStorage.setItem(EstablishmentConstants.LOCALSTORAGE_BACKUP_ESTABLISHMENT, stringObj)
    state.backupEstablishment = objectBackupEstablishment
  },

  setLogged (state, booleanValue) {
    if (state.backupEstablishment) {
      state.logged = localStorage.setItem(EstablishmentConstants.LOCALSTORAGE_LOGGED, booleanValue)
    }
  }
}

export default {
  namespaced: true, 
  state,
  getters,
  mutations
}