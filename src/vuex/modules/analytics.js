const state = {
  lastUrl: ""
}

const getters = {
  lastUrl: state => state.lastUrl
}

const mutations = {
  saveCurrentUrl (state, url) {
    // code from google analytics
    ga('set', 'page', url)
    ga('send', 'pageview') 
    console.log("analytics updated!")
  } 
}

export default {
  namespaced: true, 
  state,
  getters,
  mutations
}