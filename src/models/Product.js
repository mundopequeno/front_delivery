export default class Product {
  constructor (nome, quantidade) {
    if (!nome || !quantidade) throw new Error('Nome e quantidade deve ser informado')
    this.name = nome
    this.quantity = quantidade
  }

  get json () {
    return {
      name: this.nome,
      quantity: this.quantidade
    }
  }
}