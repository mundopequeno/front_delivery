const GROUP_TIPOS_PERMITIDOS = [
  'sacola', 'caixa'
]

/** 
 * @class Group
 * @method listGroups
*/
export default class Group {
  constructor (tipo) {
    if (GROUP_TIPOS_PERMITIDOS.indexOf(tipo) == -1) throw new Error('Tipo não suportado')
    this.type = tipo
    this.listItems = []
  }

  /**
   * returna dados da classe em formato json
   * @returns {Object}
   */
  get json () {
    return {
      type: this.tipo,
      products: this.products
    }
  }

  static get listGroups () {
    return GROUP_TIPOS_PERMITIDOS
  }

  /**
   * Adiciona produto no grupo
   * @param {Product} product
   * @returns {Array<Object>} 
   */
  addProduct (product) {
    this.listItems.push(product)
    return this.listItems
  }
}