import VueApollo from 'vue-apollo'
import apolloClient from './client'


export default new VueApollo({
  defaultClient: apolloClient,
})
