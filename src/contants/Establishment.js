export default class EstablishmentConstants {
  static get LOCALSTORAGE_BACKUP_ESTABLISHMENT () {
    return "bk_establishment"
  }

  static get LOCALSTORAGE_LOGGED () {
    return "establishment_logged"
  }
}