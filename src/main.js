// GOOGLE ANALYTICS
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-104117196-1 ', 'auto');
ga('send', 'pageview');
// END GOOGLE ANALYTICS

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueApollo from 'vue-apollo'
import App from './App'
import VueResource from 'vue-resource'
import router from './router'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import store from './vuex/index'
import apolloProvider from './graphql/provider'

// use material design
Vue.use(VueMaterial)

// use vue resource
Vue.use(VueResource)

// Install the vue plugin
Vue.use(VueApollo)

// vue config production tip
Vue.config.productionTip = false

/* eslint-disable no-new */
import gql from 'graphql-tag'

new Vue({
  el: '#app',
  router,
  store,
  apolloProvider, 
  apollo: {
    // clients: gql`{clients}`,
  }, 
  template: '<App/>',
  components: { App },
  mounted () {}
})
